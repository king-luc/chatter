set(PROJECTHEADERS
        Server.hpp
        ClientHandle.hpp
        Packets/Packet.hpp
        Packets/PacketID.hpp)

set(PROJECTSOURCES
        Main.cpp
        Server.cpp
        ClientHandle.cpp
        Packets/Packet.cpp)

find_package(Qt5Core CONFIG REQUIRED)
find_package(Qt5Network CONFIG REQUIRED)

add_executable("${CMAKE_PROJECT_NAME}-Server" ${PROJECTHEADERS} ${PROJECTSOURCES})

target_link_libraries("${CMAKE_PROJECT_NAME}-Server"
        Qt5::Core
        Qt5::Network)