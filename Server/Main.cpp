//
// Created by Luc Arne Wengoborski on 14.10.2018.
//
// Copyright 2018 Luc Arne Wengoborski
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the 
// Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
// ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH 
// THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "Server.hpp"
#include <QCoreApplication>
#include <QDebug>
#include <iostream>

int StartUp()
{
    char RawBuffer[16] {};
    bool ConversionSuccess = false;
    do{
        std::cout << "Please enter the port to listen to: ";
        std::cin.getline(RawBuffer, 16);
        QString Buffer = RawBuffer;


        int Port = Buffer.toInt(&ConversionSuccess);
        if(!ConversionSuccess)
        {
            qCritical() << "Please enter a correct number";
        } else{
            return Port;
        }
    }while(!ConversionSuccess);
}

int main(int argc, char* argv[])
{
    QCoreApplication Application(argc, argv);

    // Set up the server environment
    int Port = StartUp();

    cServer MyServer(Port);

    return QCoreApplication::exec();
}