//
// Created by Luc Arne Wengoborski on 14.10.2018.
//
// Copyright 2018 Luc Arne Wengoborski
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the 
// Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
// and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
// ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH 
// THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#include "Server.hpp"
#include <QDebug>
#include <QTcpSocket>

cServer::cServer(int aPort):
    m_Port(aPort)
{
    // Try to listen to the specified port
    if(!listen(QHostAddress::Any, aPort))
    {
        qCritical() << "Server is unable to listen to port " << aPort;
        return;
    }

    // Connect all signals and slots
    connect(this, SIGNAL(newConnection()), this, SLOT(OnNewConnection()));
    qInfo() << "Server is now listening to port " << aPort;
}

cServer::~cServer()
{
    // Delete all handles
    for(auto & Handle : m_ClientHandles)
    {
        delete Handle;
        Handle = nullptr;
    }
}

void cServer::OnNewConnection()
{
    while(hasPendingConnections())
    {
        auto Socket = nextPendingConnection();
        qInfo() << "New incoming connection from " << Socket->peerAddress().toString();
        // Create the new ClientHandle
        auto* TempHandle = new cClientHandle(Socket);
        m_ClientHandles.push_back(TempHandle);
    }
}
